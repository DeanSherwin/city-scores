from tornado.web import RequestHandler
from http import HTTPStatus
import tornado.escape
import tornado.gen as gen
import tornado.httpclient as httpclient
import tornado.web
import json
import csv
import re
from .city import City


class LocationHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    @gen.coroutine
    def get(self, city_req):
        """
        Retrieve information about a city.
        :param city_req:
        :return:
        """
        city = self.get_city(city_req)
        if city:
            self.set_status(HTTPStatus.OK)
            response = city.__dict__
            weather = yield city.weather()
            response['current_temperature'] = weather['temp']
            response['current_weather_description'] = weather['description']
            response['score'] = city.score()
            self.write(json.dumps(response))
        else:
            self.set_status(HTTPStatus.BAD_REQUEST)

    @classmethod
    def get_city(cls, city_req):
        """
        Create an instance of City using data found in the stored CSV file
        :param city_req - the city thats information is requested:
        :return City instance:
        """
        # find city in csv and extract stored data
        with open('./data/european_cities.csv', 'rt') as cities_csv:
            regex = re.compile('[^a-zA-Z\d]')
            reader = csv.DictReader(cities_csv)
            for row in reader:
                # remove non alpha chars and make city name lowercase
                city_name = regex.sub('', row['city'].lower())
                if city_req == city_name:
                    return City(
                        row['city'],
                        row['country'],
                        int(row['population']),
                        int(row['bars']),
                        int(row['museums']),
                        int(row['public_transport']),
                        int(row['crime_rate']),
                        float(row['average_hotel_cost'])
                    )
            return None
