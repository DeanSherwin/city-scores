from tornado.web import RequestHandler
from http import HTTPStatus
import json
from .location_data_handler import LocationHandler
import tornado.gen as gen
import operator



class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    @gen.coroutine
    def get(self, *args, **kwargs):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for each city.
        :param args:
        :param kwargs:
        :return:
        """
        cities = args[0].split(',')
        city_profiles = []

        for c in cities:
            city = LocationHandler.get_city(c)
            city_profiles.append({
                "city_name": city.city_name,
                "city_score": city.score()
            })

        city_profiles.sort(key=operator.itemgetter('city_score'), reverse=True)

        for idx, p in enumerate(city_profiles):
            p['rank'] = idx + 1

        response = {
            'city_data': [c for c in city_profiles]
        }

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
