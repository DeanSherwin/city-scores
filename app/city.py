from http import HTTPStatus
import tornado.gen as gen
import tornado.httpclient as httpclient
import tornado.web
import json
import csv
import re
import pandas as pd

GOOGLE_API_KEY = 'AIzaSyC43GKvUcKBAL6uCNnyl8J_tj1wz71j2_8'

class City:

    def __init__(self, name, country, population, bar_count, museums, public_transport,
        crime_rate, average_hotel_cost):
        self.city_name = name
        self.country = country
        self.population = population
        self.bar_count = bar_count
        self.museums = museums
        self.public_transport = public_transport
        self.crime_rate = crime_rate
        self.average_hotel_cost = average_hotel_cost

    def __str__(self):
        return self.city_name

    def score(self):
        """
        Given a list of city profiles, return a list of city scores 0-10
        :param cities:
        :return scores
        """
        # create dataframe and normalize data into comparable values
        cities = pd.read_csv('./data/european_cities.csv')
        # validate data by dropping any rows with missing data in CSV calued by Unicode issues
        cities = cities[cities.average_hotel_cost.notnull()]
        cities['bars_per_capita'] = cities['bars'].divide(cities['population'])
        cities['museums_per_capita'] = cities['museums'].divide(cities['population'])
        bars_z_score = ((self.bar_count / self.population) - cities['bars_per_capita'].mean()) / cities['bars_per_capita'].std()
        museum_z_score = ((self.museums / self.population) - cities['museums_per_capita'].mean()) / cities['museums_per_capita'].std()
        hotel_z_score = (self.average_hotel_cost - cities['average_hotel_cost'].mean()) / cities['average_hotel_cost'].std()
        # museum and bar scores are out of 5 (for each section of standard deviation bell curve) converted to decimal
        # 3 is average. For every n standard deviations add or subtract a point
        bar_score = (3 + int(bars_z_score)) / 5
        museum_score = (3 + int(museum_z_score)) / 5
        # hotels cheapest is better so need 1 - k where k is the hotel decimal ranking on pricing
        hotel_score = 1 - (3 + int(hotel_z_score)) / 5
        # calculate remainder of scores as decimal values
        crime_score = (10 - self.crime_rate) / 10
        transport_score = self.public_transport / 10
        # return avergae of all scores as a rounded int 0-10
        dec_score = (sum([bar_score, museum_score, hotel_score, crime_score, transport_score]) / 5) * 10
        return round(dec_score, 2)

    @gen.coroutine
    def weather(self):
        """
        Get a plain description of current weather and a socre 0-10
        :return dictionary containing description and weather score:
        """
        # get city long and lat, then use that to get weather
        # not using city name to get WOEID from weather API straight off
        # as many small cities are not accessible by name but Google can find them
        weather = dict()
        try:
            client = httpclient.AsyncHTTPClient()
            long_lat_query = '{}+{}'.format(self.city_name, self.country).replace(' ', '+').lower()
            long_lat_resp = yield client.fetch(
                'https://maps.googleapis.com/maps/api/geocode/json?address={}&key={}'
                .format(long_lat_query, GOOGLE_API_KEY))
            long_lat_json = tornado.escape.json_decode(long_lat_resp.body)

            if long_lat_json['results']:
                long_lat = (long_lat_json['results'][0]['geometry']['location'])
                # get nearest location id for given coordinates
                location_id_resp = yield client.fetch(
                    'https://www.metaweather.com/api/location/search/?lattlong={},{}'
                    .format(long_lat['lat'], long_lat['lng'])
                )
                # nearest city will be first in results list - get its WOE (Where on Earth) Id
                woe_id = int(tornado.escape.json_decode(location_id_resp.body)[0]['woeid'])
                weather_resp = yield client.fetch(
                    'https://www.metaweather.com/api/location/{}/'
                    .format(woe_id)
                )
                weather_json = tornado.escape.json_decode(weather_resp.body)['consolidated_weather'][0]
                weather['description'] = weather_json['weather_state_name']
                weather['temp'] = weather_json['the_temp']
            else:
                # manually raise 400 as Google will always return 200 with error details
                raise httpclient.HTTPError(400, long_lat_json['error_message'])
        except httpclient.HTTPError as e:
            print(e)
            weather = None
        return weather
