
# To run locally
```
virtualenv venv -p python3
source venv/bin/activate
pip install -r requirements.txt
pysthon location_api.py
```


# Notes

- Having some trouble getting the tests to run. Import error. I think its a PYTHONPATH issue. Not 100% sure how tests are ran in tornado

- The CSV had a few odd characters. I think it was a ascii -> unicode issue (Eg. Russian cities often had a crylic character replaced with a '?'). Handled this when creating Pandas dataframe

- Tried to use async requests as best I could with the API

- Used google API to get WOEID - lesser known cities could not be found directly using the Weather API so needed Long and Lat

- GOOGLE API key is hardcoded - usually I'd set up env variables
